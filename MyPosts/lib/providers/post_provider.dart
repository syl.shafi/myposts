import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/post.dart';

class PostProvider with ChangeNotifier {

  List<Post> posts = new List<Post>();

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<bool> getPots(int currentLength) async {
    
    posts = new List<Post>();

    bool success = await FirebaseFirestore.instance
        .collection('posts')
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        //print(doc.data());
        posts.add(Post(
            doc.id,
            doc.data()["title"],
            doc.data()["description"],
            doc.data()["imageUri"],
            doc.data()["creatorId"]));
      });

      notifyListeners();

      return true;
    }).catchError((error) => (value) {
              return false;
            });

    return success;
  }

  Future<bool> addPost(Post post, File image) async {

    StorageReference storageReference =
        FirebaseStorage.instance.ref().child('images/${basename(image.path)}');

    StorageUploadTask uploadTask = storageReference.putFile(image);
    await uploadTask.onComplete;
    print('File Uploaded');

    await storageReference.getDownloadURL().then((fileURL) {
      post.imageUri = fileURL;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();

    post.creatorId = prefs.getString("userId").toString();

    CollectionReference postCollection =
        FirebaseFirestore.instance.collection('posts');

    bool success = await postCollection.add({
      'title': post.title,
      'description': post.description,
      'imageUri': post.imageUri,
      'creatorId': post.creatorId
    }).then((value) {
      posts.add(new Post(value.id, post.title, post.description, post.imageUri,
          post.creatorId));

      notifyListeners();

      return true;
    }).catchError((error) => (value) {
          return false;
        });

    return success;

  }

  Future<bool> editPost(Post post, File image) async {

    if (image != null) {
      StorageReference storageReference = FirebaseStorage.instance
          .ref()
          .child('images/${basename(image.path)}');

      StorageUploadTask uploadTask = storageReference.putFile(image);
      await uploadTask.onComplete;
      print('File Uploaded');

      await storageReference.getDownloadURL().then((fileURL) {
        post.imageUri = fileURL;
      });
    }else{

      int index = posts.indexWhere((p) => p.id == post.id);
      post.imageUri = posts.elementAt(index).imageUri;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();

    post.creatorId = prefs.getString("userId").toString();

    CollectionReference postCollection =
        FirebaseFirestore.instance.collection('posts');

    bool success = await postCollection.doc(post.id).update({
      'title': post.title,
      'description': post.description,
      'imageUri': post.imageUri,
      'creatorId': post.creatorId
    }).then((value) {
      int index = posts.indexWhere((p) => p.id == post.id);

      if (index != -1) {
        posts.replaceRange(index, index + 1, [
          new Post(post.id, post.title, post.description, post.imageUri, post.creatorId)
        ]);
      }

      notifyListeners();

      return true;
    }).catchError((error) {
      return false;
    });

    return success;

  }

  Future<bool> deletePost(String id) async {

    CollectionReference postCollection =
        FirebaseFirestore.instance.collection('posts');

    bool success = await postCollection.doc(id).delete().then((value) {
      posts.removeWhere((item) => item.id == id);
      notifyListeners();

      return true;
    }).catchError((error) {
      return false;
    });

    return success;
  }
}
