import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/user.dart';

class AuthProvider with ChangeNotifier {

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<bool> login(User user) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
   
    String userId = "";


    bool success = await firestore
        .collection("users")
        .where("username", isEqualTo: user.email)
        .where("password", isEqualTo: user.pass)
        .get()
        .then((value) {
      value.docs.forEach((result) {
        print(result.data());

        int secounds = 3600;

        DateTime expiresIn = DateTime.now().add(Duration(seconds: secounds));

        prefs.setString("userId", result.id);
        prefs.setString("expiresIn", expiresIn.toString());

        userId = result.id;

      });

      notifyListeners();

      return userId == "" ? false : true;
    }).catchError((error) {
      return false;
    });

    return success;
  }

  Future<int> expiresIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    DateTime currentTime = DateTime.now();
    DateTime expiresIn = DateTime.parse(prefs.getString("expiresIn"));

    int secounds = expiresIn.difference(currentTime).inSeconds;

    return secounds > 0 ? secounds : 0;
  }

  Future<bool> isAuthorized() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userId") != null ? true : false;
  }

  Future<String> getCurrentUserId() async {
    bool isAuth = await isAuthorized();

    if (!isAuth) {
      return null;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userId");
  }

  Future<bool> signup(User user) async {

    bool userExistOrConnectionProblem = await firestore
        .collection("users")
        .where("username", isEqualTo: user.email)
        .get()
        .then((value) {
      int i = 0;
      value.docs.forEach((result) {
        print(result.data());
        i++;
      });

      if (i > 0) {
        return true;
      }

      return false;
    }).catchError((error) {
      return true;
    });

    if (userExistOrConnectionProblem) {
      return false;
    } else {


      bool success = await firestore.collection('users').add({
        'username': user.email,
        'password': user.pass,
      }).then((value) {

        notifyListeners();

        return true;

      }).catchError((error) => (value) {
            return false;
          });

      return success;
    }
  }

  logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("userId");
    prefs.remove("expiresIn");
    notifyListeners();
  }
}
