class Post{

  Post(this.id, this.title, this.description, this.imageUri, this.creatorId);
  final String id;
  final String title;
  final String description;
  String imageUri;
  String creatorId;  

}