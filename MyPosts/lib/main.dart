import 'dart:async';
import 'package:MyPosts/widgets/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';
import './widgets/login_menu.dart';
import './widgets/add_post.dart';
import './providers/post_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './providers/auth_provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  SharedPreferences.setMockInitialValues({});
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => PostProvider()),
      ChangeNotifierProvider(create: (context) => AuthProvider())
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Posts',
      initialRoute: '/',
      routes: {
        '/login': (context) => Login(),
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      navigatorObservers: <NavigatorObserver>[observer],
      home: MyHomePage(
        title: 'My Posts',
        analytics: analytics,
        observer: observer,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.analytics, this.observer})
      : super(key: key);

  final String title;
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  @override
  _MyHomePageState createState() => _MyHomePageState(analytics, observer);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState(this.analytics, this.observer);

  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;

  Future<void> _sendAnalyticsEvent() async {
    await analytics.logEvent(
      name: 'test_event',
      parameters: <String, dynamic>{
        'string': 'shafi creates an event',
        'int': 42,
        'long': 12345678910,
        'double': 42.0,
        'bool': true,
      },
    );

    print("analytical message sent.");
  }

  bool isAuthorized = false;
  String currrentUserId;
  Timer _timer;
  int currentLength = 3;
  bool isLoading = false;
  PostProvider postProvider;
  AuthProvider authProvider;

  @override
  void initState() {
    super.initState();
    _sendAnalyticsEvent();
  }

  void _initializeTimer() async {
    bool checkIsAuthorized = await authProvider.isAuthorized();

    if (checkIsAuthorized) {
      int remainingSecounds = await authProvider.expiresIn();
      _timer = Timer.periodic(
          Duration(seconds: remainingSecounds), (_) => autoLogout());
    }
  }

  void autoLogout() async {
    authProvider.logout();
    _timer.cancel();
    isAuthorized = false;
    currrentUserId = null;
    currentLength = 3;
    isLoading = false;

    await loadMore();

  }

  void getAuthorized() async {
    isAuthorized = await authProvider.isAuthorized();
  }

  void getCurrentUserId() async {
    currrentUserId = await authProvider.getCurrentUserId();
  }

  loadMore() async {
    if (!isLoading) {
      isLoading = true;
      currentLength = currentLength + 2;
      await postProvider.getPots(currentLength);
      currentLength = postProvider.posts.length;
      isLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {

    postProvider = Provider.of<PostProvider>(context, listen: false);
    authProvider = Provider.of<AuthProvider>(context, listen: false);

    if (postProvider.posts.length == 0) {
      postProvider.getPots(currentLength);
    } else {
      currentLength = postProvider.posts.length;
    }

    getAuthorized();
    getCurrentUserId();
    _initializeTimer();

    print("hellow");

    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Text(widget.title),
              Spacer(),
              LoginMenu(),
            ],
          ),
        ),
        body: Container(
            height: MediaQuery.of(context).size.height * 0.7,
            width: MediaQuery.of(context).size.width,
            child:
                Consumer<PostProvider>(builder: (context, postProvider, child) {
              return LazyLoadScrollView(
                  isLoading: isLoading,
                  onEndOfPage: loadMore,
                  child: ListView.builder(
                    itemCount: postProvider.posts.length,
                    itemBuilder: (BuildContext ctx, int index) {
                      print("sub hellow");

                      return Container(
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: new BorderSide(color: Colors.grey)),
                          ),
                          child: ListTile(
                            onTap: () async {
                              currrentUserId =
                                  await authProvider.getCurrentUserId();

                              if (currrentUserId ==
                                  postProvider.posts[index].creatorId) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddPost(
                                            id: postProvider.posts[index].id,
                                            title:
                                                postProvider.posts[index].title,
                                            description: postProvider
                                                .posts[index].description,
                                            imagePath: postProvider
                                                .posts[index].imageUri,
                                          )),
                                );
                              } else {
                                showDialog<void>(
                                  context: context,
                                  barrierDismissible:
                                      false, // user must tap button!
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title:
                                          Text(postProvider.posts[index].title),
                                      content: SingleChildScrollView(
                                        child: ListBody(
                                          children: <Widget>[
                                            Center(
                                              child: Padding(
                                                padding: EdgeInsets.all(4),
                                                child: Image.network(
                                                  postProvider
                                                      .posts[index].imageUri,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.5,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.3,
                                                ),
                                              ),
                                            ),
                                            Text(postProvider
                                                .posts[index].description),
                                          ],
                                        ),
                                      ),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text('OK'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              }
                            },
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 10.0),
                            leading: postProvider.posts[index].imageUri != ""
                                ? Image.network(
                                    postProvider.posts[index].imageUri)
                                : Icon(Icons.image),
                            title: Text(postProvider.posts[index].title),
                            trailing: Consumer<AuthProvider>(
                                builder: (context, authProvider, child) {
                              return FutureBuilder<String>(
                                  future: authProvider.getCurrentUserId(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<String> snapshot) {
                                    if (snapshot.hasData) {
                                      currrentUserId = snapshot.data;
                                      return currrentUserId ==
                                              postProvider
                                                  .posts[index].creatorId
                                          ? IconButton(
                                              icon: Icon(Icons.delete),
                                              onPressed: () async {
                                                bool success =
                                                    await postProvider
                                                        .deletePost(postProvider
                                                            .posts[index].id);
                                              },
                                            )
                                          : SizedBox();
                                    }

                                    return SizedBox();
                                  });
                            }),
                          ));
                    },
                  ));
            })),
        floatingActionButton:
            Consumer<AuthProvider>(builder: (context, authProvider, child) {
          return FutureBuilder<bool>(
              future: authProvider.isAuthorized(),
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data
                      ? FloatingActionButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddPost()),
                            );
                          },
                          tooltip: 'Add Post',
                          child: Icon(Icons.add),
                        )
                      : SizedBox();
                }
                return SizedBox();
              });
        }));
  }
}
