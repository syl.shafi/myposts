import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/post.dart';
import '../providers/post_provider.dart';

class AddPost extends StatefulWidget {
  String id;
  String title = "";
  String description = "";
  String imagePath = "";

  AddPost({Key key, this.id, this.title, this.description, this.imagePath})
      : super(key: key);

  @override
  AddPostState createState() {
    return AddPostState();
  }
}

class AddPostState extends State<AddPost> {
  final _formKey = GlobalKey<FormState>();
  String title = "";
  String description = "";
  File image;
  final picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    PostProvider postProvider =
        Provider.of<PostProvider>(context, listen: false);

    return Scaffold(
        appBar: AppBar(
          title: Text("Add Post"),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    initialValue: widget.title,
                    validator: (value) {
                      title = value;

                      if (value.isEmpty) {
                        return 'Please enter title';
                      }
                      return null;
                    },
                    decoration: InputDecoration(hintText: "Title"),
                  ),
                  TextFormField(
                    initialValue: widget.description,
                    validator: (value) {
                      description = value;

                      if (value.isEmpty) {
                        return 'Please enter description';
                      }
                      return null;
                    },
                    decoration: InputDecoration(hintText: "Description"),
                  ),
                  image != null
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Image.file(
                              image,
                              width: MediaQuery.of(context).size.width * 0.5,
                              height: MediaQuery.of(context).size.height * 0.2,
                            ),
                          ),
                        )
                      : widget.imagePath != null
                          ? Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.network(
                                  widget.imagePath,
                                  width:
                                      MediaQuery.of(context).size.width * 0.5,
                                  height:
                                      MediaQuery.of(context).size.height * 0.2,
                                ),
                              ),
                            )
                          : SizedBox(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Center(
                      child: FlatButton(
                        onPressed: () async {
                          final pickedFile =
                              await picker.getImage(source: ImageSource.camera);

                          if (pickedFile == null) {
                            return;
                          }

                          setState(() {
                            image = File(pickedFile.path);
                          });
                        },
                        child: Text(
                          'Pick Image',
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: FlatButton(
                      onPressed: () async {
                        if (_formKey.currentState.validate() &&
                            widget.id == null &&
                            image != null) {
                          bool success = await postProvider.addPost(
                              Post("", title, description, "", ""), image);

                          if (success) {
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Navigator.pushNamed(context, '/');
                            });
                          }
                        } else if (_formKey.currentState.validate() &&
                            widget.id != null) {
                          bool success = await postProvider.editPost(
                              Post(widget.id, title, description, "", ""),
                              image);

                          print(success);

                          if (success) {
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Navigator.pushNamed(context, '/');
                            });
                          }
                        }
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
