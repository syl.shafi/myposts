import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './signup.dart';
import './login.dart';
import '../providers/auth_provider.dart';

class LoginMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

  AuthProvider authProvider = Provider.of<AuthProvider>(context);


    return Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: FutureBuilder<bool>(
          future: authProvider.isAuthorized(), // async work
          builder: (BuildContext context, AsyncSnapshot<bool> asyncSnapshot) {
            if (asyncSnapshot.hasData) {
              if (!asyncSnapshot.data) {
                return Row(
                  children: <Widget>[
                    Spacer(),
                    InkWell(
                      child: Text(
                        "login",
                        style: TextStyle(fontSize: 16),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Login()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      child: Text(
                        "Signup",
                        style: TextStyle(fontSize: 16),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Signup()),
                        );
                      },
                    )
                  ],
                );
              } else {
                return Row(children: <Widget>[
                  Spacer(),
                  InkWell(
                    child: Text(
                      "logout",
                      style: TextStyle(fontSize: 16),
                    ),
                    onTap: () async{
                     await authProvider.logout();
                    },
                  ),
                ]);
              }
            }else{
              return Row(
                children: <Widget>[
                  SizedBox()
                ],
              );
            }
          },
        ));
  }
}
